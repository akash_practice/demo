//const { Router } = require('express')
const express = require('express')
//const { request } = require('http')

const db = require('../db')

const router = express.Router()


// 1.Add Student 

router.post('/akash',(request,response)=>
{
    const { roll_no,name ,Class,division,dateofbirth,parent_mobile_no} = request.body
 
    const statement =
    `INSERT into student 
    (roll_no,name,Class,division,dateofbirth,parent_mobile_no)
     values
       ('${roll_no}','${name}', '${Class}','${division}','${dateofbirth}','${parent_mobile_no}');
    `
    const connection= db.connect()
    connection.query(statement,(error,result)=>
    {
        connection.end()

        if (error)
        {
            response.send(error)
        }
        else{
            response.send(result)
        }
        
    })

})


// 2.Find Student by rollno 

router.get('/akash/:roll_no',(request,response)=>
{
    const { roll_no} = request.params
 
    const statement =
    `
    SELECT * from student 
    WHERE
    roll_no=${roll_no};
    `
    const connection= db.connect()
    connection.query(statement,(error,result)=>
    {
        connection.end()

        if (error)
        {
            response.send(error)
        }
        else{
            response.send(result)
        }
        
    })

})

// 3.Edit specific Student's class and division 
router.put('/akash/:roll_no',(request,response)=>
{
    const { roll_no} = request.params

    const {Class,division} = request.body


    const statement =
    `
    UPDATE student SET
    Class = '${Class}',
    division = '${division}'
    WHERE
    roll_no=${roll_no};
    `
    const connection= db.connect()
    connection.query(statement,(error,result)=>
    {
        connection.end()

        if (error)
        {
            response.send(error)
        }
        else{
            response.send(result)
        }
        
    })

})

//4. Delete Student 
router.delete('/akash/:roll_no',(request,response)=>
{
    const {roll_no} = request.params
    const statement =
    `
    DELETE from student
    where
    roll_no = ${roll_no}
    `
    const connection= db.connect()
    connection.query(statement,(error,result)=>
    {
        connection.end()

        if (error)
        {
            response.send(error)
        }
        else{
            response.send(result)
        }
        
    })

})

//5. Fetch all students of particular class 
router.get('/:Class',(request,response)=>
{
    const {Class} = request.params
 
    const statement =
    `
    SELECT * from student 
    WHERE
    Class = ${Class}
    `
    const connection= db.connect()
    connection.query(statement,(error,result)=>
    {
        connection.end()

        if (error)
        {
            response.send(error)
        }
        else{
            response.send(result)
        }
        
    })

})

//6. Fetch all students of particular birth year 
router.get('/student/:year',(request,response)=>
{
    const {year} = request.params
 
    const statement =
    `
    SELECT * from student 
    WHERE
    year(dateofbirth)= ${year};
    `
    const connection= db.connect()
    connection.query(statement,(error,result)=>
    {
        connection.end()

        if (error)
        {
            response.send(error)
        }
        else{
            response.send(result)
        }
        
    })

})

module.exports=router